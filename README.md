# Dotfiles

## Overview

This repo is based off the bootstrap script from:
https://github.com/jeffaco/msft-dotfiles/blob/master/bootstrap.sh
For managing dotfiles

## Using this repo

Clone repo, then bootstrap it

    $ cd $HOME
    $ git clone --recurse-submodules https://gitlab.com/x3nn3x/dotfiles.git dotfiles
    $ cd .dotfiles
    $  # edit files
    $  # edit files
    $  # When satisfied with your files, run the boostrapper:
    $ dotfiles/bootstrap.sh

Authenticated cloning:

    $ cd $HOME
    $ git clone --recurse-submodules git@gitlab.com:x3nn3x/dotfiles.git dotfiles
    $ dotfiles/bootstrap.sh

## Compatibility

The bootstrap utility has been tested to work properly on Linux, IBM
AIX, HP-UX, and Sun Solaris.

## To update zsh
To update with new config: zgen update

## To initialise vim
Start vim and run :PluginInstall

